import React from 'react';
import connectToStores from 'alt/utils/connectToStores';

import AddSupplierPage from './AddSupplierPage';
import Breadcrumb from '../../components/Breadcrumb';
import SupplierAction from '../../actions/SupplierAction';
import SupplierList from '../../components/Supplier/SupplierList';
import SupplierStore from '../../stores/SupplierStore';
import EditSupplierPage from './EditSupplierPage';

@connectToStores
export default class SupplierPage extends React.Component {
  static contextTypes = {
    onSetTitle: React.PropTypes.func,
  };

  static getStores() {
    return [SupplierStore];
  }

  static getPropsFromStores() {
    return {
      supplierStore: SupplierStore.getState(),
    };
  }

  componentWillMount() {
    this.context.onSetTitle('Supplier');
    SupplierAction.fetch();
  }

  find(id) {
    SupplierAction.find(id);
  }

  onEdit(id) {
    this.props.history.replaceState(null, `/supplier/${id}`);
  }

  onSave(id, payload) {
    if (!id) {
      SupplierAction.create(payload);
    } else {
      SupplierAction.update(id, payload);
    }
  }

  onRemove(id) {
    if (confirm('Are you sure to remove supplier?')) {
      SupplierAction.delete(id);
    }
  }

  render() {
    const { children, supplierStore, find, onEdit, onSave, onRemove } = this.props;
    if (children) {
      return React.cloneElement(children, {
        find,
        onEdit,
        onSave,
        onRemove,
      });
    }
    const breadcrumbs = [
      { icon: 'fa-home', location: '/', title: 'Home' },
      { icon: 'fa-table', location: '/supplier', title: 'Supplier' },
    ];
    return (
      <div className="SupplierPage">
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">Supplier</h1>
            <Breadcrumb items={breadcrumbs} />
            <SupplierList
              onEdit={this.onEdit}
              onRemove={this.onRemove}
              supplierList={supplierStore.suppliers}
            />
          </div>
        </div>
      </div>
    );
  }
}

SupplierPage.Add = AddSupplierPage;
SupplierPage.Edit = EditSupplierPage;
