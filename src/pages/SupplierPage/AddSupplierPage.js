import React from 'react';
import Breadcrumb from '../../components/Breadcrumb';

export default class AddSupplierPage extends React.Component {
  static contextTypes = {
    onSetTitle: React.PropTypes.func,
  };

  componentWillMount() {
    this.context.onSetTitle('Supplier - Add');
  }

  render() {
    const breadcrumbs = [
      { icon: 'fa-home', location: '/', title: 'Home' },
      { icon: 'fa-table', location: '/supplier', title: 'Supplier' },
      { icon: 'fa-plus', location: '/supplier/add', title: 'Add' },
    ];
    return (
      <div className="AddSupplierPage">
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">Add Supplier</h1>
            <Breadcrumb items={breadcrumbs} />
          </div>
        </div>
      </div>
    );
  }
}