import React from 'react';
import Breadcrumb from '../../components/Breadcrumb';

export default class EditSupplierPage extends React.Component {
  static contextTypes = {
    onSetTitle: React.PropTypes.func,
  };

  componentWillMount() {
    this.context.onSetTitle('Supplier - Edit');
  }

  render() {
    const breadcrumbs = [
      { icon: 'fa-home', location: '/', title: 'Home' },
      { icon: 'fa-table', location: '/supplier', title: 'Supplier' },
      { icon: 'fa-pencil', location: '/supplier/edit', title: 'Edit' },
    ];
    return (
      <div className="EditSupplierPage">
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">Edit Supplier</h1>
            <Breadcrumb items={breadcrumbs} />
          </div>
        </div>
      </div>
    );
  }
}