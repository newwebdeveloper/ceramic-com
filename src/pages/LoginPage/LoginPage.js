import React from 'react';
import connectToStores from 'alt/utils/connectToStores';
import AuthenticationAction from '../../actions/AuthenticationAction';
import AuthenticationStore from '../../stores/AuthenticationStore';
import DangerAlert from '../../components/Alerts/DangerAlert';
import SuccessAlert from '../../components/Alerts/SuccessAlert';

export default class LoginPage extends React.Component {
  static contextTypes = {
    onSetTitle: React.PropTypes.func,
  };

  state = {
    username: '',
    password: '',
  };

  constructor() {
    super();
    this.onLogin = this.onLogin.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }

  componentWillMount() {
    this.context.onSetTitle('Login');
  }

  componentWillReceiveProps(nextProps) {
    const authenticationStore = this.props.authenticationStore;
    const nextAuthenticationStore = nextProps.authenticationStore;
    if (nextAuthenticationStore.accessToken && nextAuthenticationStore.accessToken != authenticationStore.accessToken) {
      this.props.history.replaceState(null, '/');
    }
  }

  onInputChange(event) {
    const target = event.target;
    this.setState({ [target.name]: target.value });
  }

  onLogin(event) {
    const { username, password } = this.state;
    AuthenticationAction.login(username, password);
    event.preventDefault();
  }

  render() {
    const authenticationStore = this.props.authenticationStore;

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-4 col-md-offset-4">
            <div className="login-panel panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">Please Sign In</h3>
              </div>
              <div className="panel-body">
                <form role="form" onSubmit={this.onLogin}>
                  <fieldset>
                    <div className="form-group">
                      <input className="form-control" placeholder="Username" name="username" type="username" autofocus required value={this.state.username} onChange={this.onInputChange} />
                    </div>
                    <div className="form-group">
                      <input className="form-control" placeholder="Password" name="password" type="password" required value={this.state.password} onChange={this.onInputChange} />
                    </div>
                    <div className="checkbox">
                      <label>
                        <input name="remember" type="checkbox" value="Remember Me" />Remember Me
                      </label>
                    </div>
                    <DangerAlert message={authenticationStore.errorMessage} />
                    <SuccessAlert message={authenticationStore.successMessage} />
                    <input type="submit" className="btn btn-lg btn-success btn-block" value="Login" disabled={authenticationStore.loading} />
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
