import React from 'react';
import Breadcrumb from '../../components/Breadcrumb';
import CustomerForm from '../../components/Customer/CustomerForm';

export default class EditCustomerPage extends React.Component {
  static contextTypes = {
    onSetTitle: React.PropTypes.func
  };

  componentWillMount() {
    this.context.onSetTitle('Customer - Edit');
  }

  render() {
    var id = this.props.params.customerId,
        customerData = this.props.find(id);

    const breadcrumbs = [
      { icon: 'fa-home', location: '/', title: 'Home' },
      { icon: 'fa-table', location: '/customer', title: 'Customer' },
      { icon: 'fa-pencil', location: '/customer/edit', title: 'Edit' },
    ];
    return (
      <div className="EditCustomerPage">
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">Edit Customer</h1>
            <Breadcrumb items={breadcrumbs} />
            <CustomerForm 
              customerData={customerData}
              onSave={this.props.onSave}
            />
          </div>
        </div>
      </div>
    );
  }
}