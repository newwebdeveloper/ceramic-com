import React from 'react';
import _ from 'lodash';
import connectToStores from 'alt/utils/connectToStores';

import AddCustomerPage from './AddCustomerPage';
import Breadcrumb from '../../components/Breadcrumb';
import CustomerAction from '../../actions/CustomerAction';
import CustomerList from '../../components/Customer/CustomerList';
import CustomerStore from '../../stores/CustomerStore';
import EditCustomerPage from './EditCustomerPage';

@connectToStores
export default class CustomerPage extends React.Component {

  static contextTypes = {
    onSetTitle: React.PropTypes.func,
    location: React.PropTypes.object,
    history: React.PropTypes.object,
  };

  static getStores() {
    return [CustomerStore];
  }

  static getPropsFromStores() {
    return {
      customerStore: CustomerStore.getState(),
    };
  }

  componentWillMount() {
    this.context.onSetTitle('Customer');
    CustomerAction.fetch();
  }

  find(id) {
    var customers = CustomerStore.getState().customers;
    return _.find(customers, (customer) => customer.customerId == id);
  }

  onAdd() {
    this.context.history.pushState(null, `/customer/add`);
  }

  onEdit(id) {
    this.context.history.pushState(null, `/customer/edit/${id}`);
  }

  onSave(id, payload) {
    if (!id) {
      CustomerAction.create(payload);
    } else {
      CustomerAction.update(id, payload);
    }
  }

  onRemove(id) {
    if (confirm('Are you sure to remove customer?')) {
      CustomerAction.delete(id);
    }
  }

  render() {
    const { children, customerStore, find, onAdd, onEdit, onSave, onRemove } = this.props;
    if (children) {
      return React.cloneElement(children, {
        find: this.find,
        onAdd: this.onAdd,
        onEdit: this.onEdit,
        onSave: this.onSave,
        onRemove: this.onRemove,
      });
    }
    const breadcrumbs = [
      { icon: 'fa-home', location: '/', title: 'Home' },
      { icon: 'fa-table', location: '/customer', title: 'Customer' },
    ];
    return (
      <div className="CustomerPage">
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">Customer</h1>
            <Breadcrumb items={breadcrumbs} />
            <button className="btn btn-sm btn-default" onClick={this.onAdd.bind(this)}>
              <i className="glyphicon glyphicon-plus" />
            </button>
            <CustomerList
              parent={this}
              onEdit={this.onEdit}
              onRemove={this.onRemove}
              customerList={customerStore.customers}
            />
          </div>
        </div>
      </div>
    );
  }
}

CustomerPage.Add = AddCustomerPage;
CustomerPage.Edit = EditCustomerPage;