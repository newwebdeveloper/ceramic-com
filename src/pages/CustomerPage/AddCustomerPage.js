import React from 'react';
import Breadcrumb from '../../components/Breadcrumb';
import CustomerForm from '../../components/Customer/CustomerForm';

export default class AddCustomerPage extends React.Component {
  static contextTypes = {
    onSetTitle: React.PropTypes.func,
  };

  componentWillMount() {
    this.context.onSetTitle('Customer - Add');
  }

  render() {
    const breadcrumbs = [
      { icon: 'fa-home', location: '/', title: 'Home' },
      { icon: 'fa-table', location: '/customer', title: 'Customer' },
      { icon: 'fa-plus', location: '/customer/add', title: 'Add' },
    ];
    return (
      <div className="AddCustomerPage">
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">Add Customer</h1>
            <Breadcrumb items={breadcrumbs} />
            <CustomerForm
              onSave={this.props.onSave} 
            />
          </div>
        </div>
      </div>
    );
  }
}