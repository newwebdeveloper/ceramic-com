import React, { PropTypes } from 'react';

class ErrorPage extends React.Component {
	static contextTypes = {
		onSetTitle: PropTypes.func.isRequired,
		onPageNotFound: PropTypes.func
	}

	render() {
		let title = 'Error';
		this.context.onSetTitle(title);
		return (
			<div>
				<h1>{title}</h1>
				<p>Sorry, an critical error occurred on this page.</p>
			</div>
		);
	}
}

export default ErrorPage;