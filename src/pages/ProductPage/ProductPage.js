import React from 'react';

export default class Product extends React.Component {
  static contextTypes = {
    onSetTitle: React.PropTypes.func,
  };

  componentWillMount() {
    this.context.onSetTitle('Product');
  }

  render() {
    return (
      <div className="row">
        <div className="col-lg-12">
          <h1 className="page-header">Product</h1>
        </div>
        <div className="row">
        </div>
      </div>
    );
  }
}