import React, { PropTypes } from 'react';

class NotFoundPage extends React.Component {
	static contextTypes = {
		onSetTitle: PropTypes.func.isRequired,
		onPageNotFound: PropTypes.func
	}

	render() {
		let title = 'Page Not Found';
		this.context.onSetTitle(title);
		this.context.onPageNotFound();
		return (
			<div>
				<h1>{title}</h1>
				<p>Sorry, but the page you were trying to view does not exist.</p>
			</div>
		);
	}
}

export default NotFoundPage;