import React from 'react';
import Breadcrumb from '../../components/Breadcrumb';

export default class AddStaffPage extends React.Component {
  static contextTypes = {
    onSetTitle: React.PropTypes.func,
  };

  componentWillMount() {
    this.context.onSetTitle('Staff - Add');
  }

  render() {
    const breadcrumbs = [
      { icon: 'fa-home', location: '/', title: 'Home' },
      { icon: 'fa-table', location: '/staff', title: 'Staff' },
      { icon: 'fa-plus', location: '/staff/add', title: 'Add' },
    ];
    return (
      <div className="AddStaffPage">
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">Add Staff</h1>
            <Breadcrumb items={breadcrumbs} />
          </div>
        </div>
      </div>
    );
  }
}