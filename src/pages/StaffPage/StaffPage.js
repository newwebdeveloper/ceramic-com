import React from 'react';
import connectToStores from 'alt/utils/connectToStores';

import AddStaffPage from './AddStaffPage';
import Breadcrumb from '../../components/Breadcrumb';
import StaffAction from '../../actions/StaffAction';
import StaffList from '../../components/Staff/StaffList';
import StaffStore from '../../stores/StaffStore';
import EditStaffPage from './EditStaffPage';

@connectToStores
export default class StaffPage extends React.Component {
  static contextTypes = {
    onSetTitle: React.PropTypes.func,
  };

  static getStores() {
    return [StaffStore];
  }

  static getPropsFromStores() {
    return {
      staffStore: StaffStore.getState(),
    };
  }

  componentWillMount() {
    this.context.onSetTitle('Staff');
    StaffAction.fetch();
  }

  find(id) {
    StaffAction.find(id);
  }

  onEdit(id) {
    this.props.history.replaceState(null, `/staff/${id}`);
  }

  onSave(id, payload) {
    if (!id) {
      StaffAction.create(payload);
    } else {
      StaffAction.update(id, payload);
    }
  }

  onRemove(id) {
    if (confirm('Are you sure to remove staff?')) {
      StaffAction.delete(id);
    }
  }

  render() {
    const { children, staffStore, find, onEdit, onSave, onRemove } = this.props;
    if (children) {
      return React.cloneElement(children, {
        find,
        onEdit,
        onSave,
        onRemove,
      });
    }
    const breadcrumbs = [
      { icon: 'fa-home', location: '/', title: 'Home' },
      { icon: 'fa-table', location: '/staff', title: 'Staff' },
    ];
    return (
      <div className="StaffPage">
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">Staff</h1>
            <Breadcrumb items={breadcrumbs} />
            <StaffList
              onEdit={this.onEdit}
              onRemove={this.onRemove}
              staffList={staffStore.staffs}
            />
          </div>
        </div>
      </div>
    );
  }
}

StaffPage.Add = AddStaffPage;
StaffPage.Edit = EditStaffPage;
