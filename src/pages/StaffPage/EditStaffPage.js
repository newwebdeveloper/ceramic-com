import React from 'react';
import Breadcrumb from '../../components/Breadcrumb';

export default class EditStaffPage extends React.Component {
  static contextTypes = {
    onSetTitle: React.PropTypes.func,
  };

  componentWillMount() {
    this.context.onSetTitle('Staff - Edit');
  }

  render() {
    const breadcrumbs = [
      { icon: 'fa-home', location: '/', title: 'Home' },
      { icon: 'fa-table', location: '/staff', title: 'Staff' },
      { icon: 'fa-pencil', location: '/staff/edit', title: 'Edit' },
    ];
    return (
      <div className="EditStaffPage">
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">Edit Staff</h1>
            <Breadcrumb items={breadcrumbs} />
          </div>
        </div>
      </div>
    );
  }
}