import React from 'react';

import Breadcrumb from '../../components/Breadcrumb';

export default class Home extends React.Component {
  static contextTypes = {
    onSetTitle: React.PropTypes.func,
  };

  componentWillMount() {
    this.context.onSetTitle('Home');
  }

  render() {
    const breadcrumbs = [{ icon: 'fa-home', location: '/', title: 'Home' }];
    return (
      <div className="row">
        <div className="col-lg-12">
          <h1 className="page-header">Home</h1>
          <Breadcrumb items={breadcrumbs} />
        </div>
      </div>
    );
  }
}
