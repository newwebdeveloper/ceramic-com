import Iso from 'iso';
import React from 'react';
import ReactDOM from 'react-dom';

import alt from './alt';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import routes from './routes';

import { Router } from 'react-router';

Iso.bootstrap((state, meta, container) => {
  alt.bootstrap(state);
  ReactDOM.render(<Router history={createBrowserHistory()} routes={routes} />, container);
});
