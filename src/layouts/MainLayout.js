import React from 'react';
import Header from '../components/Header';

export default class MainLayout extends React.Component {
  componentDidMount() {
    $(window).bind("load resize", () => {
        let topOffset = 50;
        const width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        if (width < 768) {
          $('div.navbar-collapse').addClass('collapse');
          topOffset = 100;
        } else {
          $('div.navbar-collapse').removeClass('collapse');
        }

        let height = ((window.innerHeight > 0) ? window.innerHeight : screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
          $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    const url = window.location;
    const element = $('ul.nav a').filter(() => {
      return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
      element.addClass('active');
    }
  }

  render() {
    return (
      <div id="wrapper">
        <Header />
        <div id="page-wrapper">
          {this.props.children}
        </div>
      </div>
    );
  }
}