import _ from 'lodash';
import React from 'react';
import moment from 'moment';
import Staff from './Staff';

export default class StaffList extends React.Component {
  static propTypes = {
    staffList: React.PropTypes.arrayOf(React.PropTypes.shape(Staff.propTypes)),
  };

  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>No</th>
            <th>Staff Name</th>
            <th>Gender</th>
            <th>Birthdate</th>
            <th>Address</th>
            <th>Phone Number</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {this.props.staffList.map((staff, index) => {
            staff.staffBirthdate = moment(this.props.staffBirthdate).format("DD-MMM-YYYY");
            const { onEdit, onRemove } = this.props;
            _.merge(staff, {
              index,
              key: `staff-${index}`,
              onEdit: onEdit.bind(staff.staffId),
              onRemove: onRemove.bind(staff.staffId),
            });
            delete staff.staffId;
            return <Staff { ...staff } />
          })}
        </tbody>
      </table>
    );
  }
}