import React from 'react';

export default class Staff extends React.Component {
  static propTypes = {
    staffName: React.PropTypes.string.isRequired,
    staffGender: React.PropTypes.string.isRequired,
    staffBirthdate: React.PropTypes.string.isRequired,
    staffAddress: React.PropTypes.string.isRequired,
    phoneNumber: React.PropTypes.string.isRequired,
  };

  render() {
    const {
      index,
      staffName,
      staffGender,
      staffBirthdate,
      staffAddress,
      phoneNumber,
      onEdit,
      onRemove,
    } = this.props;
    return (
      <tr>
        <td>{index + 1}</td>
        <td>{staffName}</td>
        <td>{staffGender}</td>
        <td>{staffBirthdate}</td>
        <td>{staffAddress}</td>
        <td>{phoneNumber}</td>
        <td>
          <button className="btn btn-sm btn-default" onClick={onEdit}>
            <i className="glyphicon glyphicon-pencil" />
          </button>
          {' '}
          <button className="btn btn-sm btn-default" onClick={onRemove}>
            <i className="glyphicon glyphicon-trash" />
          </button>
        </td>
      </tr>
    );
  }
}