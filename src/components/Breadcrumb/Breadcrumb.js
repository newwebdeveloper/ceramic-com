import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

export default class Breadcrumb extends React.Component {
  static propTypes = {
    items: React.PropTypes.arrayOf(
      React.PropTypes.shape({
        location: React.PropTypes.string.isRequired,
        title: React.PropTypes.string.isRequired,
      }),
    ),
  };

  render() {
    const items = this.props.items || [];
    return (
      <ol className="breadcrumb">
        {items.map(this.renderBreadcrumbs)}
      </ol>
    );
  }

  renderBreadcrumbs(item, index, items) {
    const key = `breadcrumb-${index}`;
    if (items.length - 1 === index) {
      return (
        <li key={key} className="active">
          <i className={classNames('fa', item.icon)} />{' '}<span>{item.title}</span>
        </li>
      );
    }
    return (
      <li key={key}>
        <Link to={item.location}>
          <i className={classNames('fa', item.icon)} />{' '}<span>{item.title}</span>
        </Link>
      </li>
    );
  }
}
