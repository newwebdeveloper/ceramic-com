import React from 'react';

export default class Customer extends React.Component {
  static propTypes = {
    customerName: React.PropTypes.string.isRequired,
    customerGender: React.PropTypes.string.isRequired,
    customerBirthdate: React.PropTypes.string.isRequired,
    customerAddress: React.PropTypes.string.isRequired,
    phoneNumber: React.PropTypes.string.isRequired,
  };

  render() {
    const {
      index,
      customerName,
      customerGender,
      customerBirthdate,
      customerAddress,
      phoneNumber,
      onEdit,
      onRemove,
    } = this.props;
    return (
      <tr>
        <td>{index + 1}</td>
        <td>{customerName}</td>
        <td>{customerGender}</td>
        <td>{customerBirthdate}</td>
        <td>{customerAddress}</td>
        <td>{phoneNumber}</td>
        <td>
          <button className="btn btn-sm btn-default" onClick={onEdit}>
            <i className="glyphicon glyphicon-pencil" />
          </button>
          {' '}
          <button className="btn btn-sm btn-default" onClick={onRemove}>
            <i className="glyphicon glyphicon-trash" />
          </button>
        </td>
      </tr>
    );
  }
}