import _ from 'lodash';
import React from 'react';
import moment from 'moment';
import Customer from './Customer';

export default class CustomerList extends React.Component {
  static propTypes = {
    customerList: React.PropTypes.arrayOf(React.PropTypes.shape(Customer.propTypes)),
  };

  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Gender</th>
            <th>Birthdate</th>
            <th>Address</th>
            <th>Phone Number</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {this.props.customerList.map((customer, index) => {
            customer.customerBirthdate = moment(customer.customerBirthdate).format("DD MMM YYYY");
            const { parent, onEdit, onRemove } = this.props;
            _.merge(customer, {
              index,
              key: `customer-${index}`,
              onEdit: onEdit.bind(parent, customer.customerId),
              onRemove: onRemove.bind(customer.customerId),
            });
            // delete customer.customerId;
            return <Customer { ...customer } />
          })}
        </tbody>
      </table>
    );
  }
}