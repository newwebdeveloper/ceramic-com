import React from 'react';
import moment from 'moment';
import DateTimeField from 'react-bootstrap-datetimepicker';

export default class CustomerForm extends React.Component {
  
  static contextTypes = {
    onSetTitle: React.PropTypes.func,
  }

  componentWillMount() {
  	this.setState({errors: {}, gender: "", submitted: {} });
  }

  onHandleSave(e) {
  	e.preventDefault();
  	var customerId = (this.refs.customerID.value == "") ? null : this.refs.customerID.value;
  	if (this.isValid()) {
  		this.state.submitted = this.getFormData();
  		this.props.onSave(customerId, this.state.submitted);
  	}
  }

  isValid() {
  	var fields = ['customerName', 'customerGender', 'customerBirthdate', 'customerAddress', 'phoneNumber'];

  	var errors = {};
  	fields.forEach(function(field) {
  		var value = (field == 'customerGender') ? this.state.gender : ((field == 'customerBirthdate') ? this.refs[field].getValue() : this.refs[field].value);
  		if (!value) {
          errors[field] = 'This field is required'
        }
  	}.bind(this))
  	this.setState({errors: errors})
  	var isValid = true
    for (var error in errors) {
      isValid = false
      break
    }
    return isValid
  }

  getFormData() {
  	var data = {
      customerName: this.refs.customerName.value
    , customerGender: this.state.gender
    , customerBirthdate: this.refs.customerBirthdate.getValue()
    , customerAddress: this.refs.customerAddress.value
    , phoneNumber: this.refs.phoneNumber.value
  	}
  	return data;
  }

  render() {
  	var data = ((typeof this.props.customerData == 'undefined') 
  					? { customerId: "", customerName: "", customerGender: "", customerBirthdate: "", customerAddress: "", phoneNumber: "" }
  					: this.props.customerData);
  	this.state.gender = (data.customerGender == "") ? "MALE" : data.customerGender;
  	return (
  	  <form role="form" className="form-horizontal" name="formCustomer" id="formCustomer" onSubmit={this.onHandleSave.bind(this)}>
  	  	  <input type="hidden" ref="customerID" name="customerID" id="customerID" defaultValue={data.customerId} />
  	  	  {this.renderTextInput("customerName", "Customer Name", data.customerName)}
  	  	  {this.renderRadioInlines("customerGender", "Gender", {
            values: ["MALE", "FEMALE"], 
            defaultCheckedValue: (data.customerGender == "") ? "MALE" : data.customerGender
          })}
          {this.renderDatePicker("customerBirthdate", "Birth Date", data.customerBirthdate)}
          {this.renderTextarea('customerAddress', 'Address', data.customerAddress)}
  	  	  {this.renderTextInput("phoneNumber", "Phone Number", data.phoneNumber, "(021) 800 800")}\
	  	  <div className="form-group">
		  	<label className="control-label col-sm-2"></label>
		  	<div className="col-sm-10">
	  	  	  <label name="errorDisplay" id="errorDisplay" style={{color:"red", display:"none"}}></label>
			</div>
		  </div>
	  	  <div className="form-group">
		  	<label className="control-label col-sm-2"></label>
		  	<div className="col-sm-10">
	  	  	  <button type="submit" className="btn btn-primary" name="btnSave" id="btnSave">Save</button>
			</div>
		  </div>
	  </form>
  	);
  }

  renderTextInput(id, label, value, placeholder) {
  	return this.renderField(id, label,
      <input type="text" className="form-control" id={id} ref={id} placeholder={((typeof placeholder == "undefined") ? label : placeholder)} defaultValue={value} />
    )
  }

  renderTextarea(id, label, value) {
  	return this.renderField(id, label,
      <textarea className="form-control" id={id} ref={id} rows="5" defaultValue={value} />
    )
  }

  renderSelect(id, label, values) {
  	var options = values.map(function(value, index) {
      return <option value={value} key={index}>{value}</option>
    })
    return this.renderField(id, label,
      <select className="form-control" id={id} ref={id}>
        {options}
      </select>
    )
  }

  onGenderChange(e) {
  	this.setState({
  		gender: e.target.value
  	});
  }

  renderRadioInlines(id, label, kwargs) {
  	var _this = this;
    var radios = kwargs.values.map(function(value, index) {
      var defaultChecked = (value == kwargs.defaultCheckedValue)
      return <label className="radio-inline" key={index}>
        <input type="radio" ref={id + value} name={id} value={value} defaultChecked={defaultChecked} onChange={_this.onGenderChange.bind(_this)} />
        {value}
      </label>
    })
    return this.renderField(id, label, radios)
  }

  renderDatePicker(id, label, value) {
  	return this.renderField(id, label,
  	  <DateTimeField ref={id}
  	  	// format="MM-DD-YYYY"
  	  	inputFormat="MM-DD-YYYY"
  	  	mode="date"
  	  	defaultText={value}
  	  	onChange={function(x){ }} />
  	)
  }

  renderField(id, label, field) {
    return <div className={$c('form-group', {'has-error': id in this.state.errors})}>
      <label htmlFor={id} className="col-sm-2 control-label">{label}</label>
      <div className="col-sm-10">
        {field}
      </div>
    </div>
  }
}

var trim = () => {
  var TRIM_RE = /^\s+|\s+$/g
  return trim = (string) => {
    return string.replace(TRIM_RE, '')
  }
}

var $c = (staticClassName, conditionalClassNames) => {
    var classNames = []
    if (typeof conditionalClassNames == 'undefined') {
      conditionalClassNames = staticClassName
    }
    else {
      classNames.push(staticClassName)
    }
    for (var className in conditionalClassNames) {
      if (!!conditionalClassNames[className]) {
        classNames.push(className)
      }
    }
    return classNames.join(' ')
  }