import React from 'react';
import classNames from 'classnames';

export default class DangerAlert extends React.Component {
  render() {
    return (
      <div className={classNames('alert', 'alert-danger', { "hide": !this.props.message })}>
        {this.props.message}
      </div>
    );
  }
}