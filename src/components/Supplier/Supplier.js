import React from 'react';

export default class Supplier extends React.Component {
  static propTypes = {
    supplierName: React.PropTypes.string.isRequired,
    supplierAddress: React.PropTypes.string.isRequired,
    phoneNumber: React.PropTypes.string.isRequired,
  };

  render() {
    const {
      index,
      supplierName,
      supplierAddress,
      phoneNumber,
      onEdit,
      onRemove,
    } = this.props;
    return (
      <tr>
        <td>{index + 1}</td>
        <td>{supplierName}</td>
        <td>{supplierAddress}</td>
        <td>{phoneNumber}</td>
        <td>
          <button className="btn btn-sm btn-default" onClick={onEdit}>
            <i className="glyphicon glyphicon-pencil" />
          </button>
          {' '}
          <button className="btn btn-sm btn-default" onClick={onRemove}>
            <i className="glyphicon glyphicon-trash" />
          </button>
        </td>
      </tr>
    );
  }
}