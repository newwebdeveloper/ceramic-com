import _ from 'lodash';
import React from 'react';
import Supplier from './Supplier';

export default class SupplierList extends React.Component {
  static propTypes = {
    supplierList: React.PropTypes.arrayOf(React.PropTypes.shape(Supplier.propTypes)),
  };

  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>No</th>
            <th>Supplier Name</th>
            <th>Address</th>
            <th>Phone Number</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {this.props.supplierList.map((supplier, index) => {
            const { onEdit, onRemove } = this.props;
            _.merge(supplier, {
              index,
              key: `supplier-${index}`,
              onEdit: onEdit.bind(supplier.supplierId),
              onRemove: onRemove.bind(supplier.supplierId),
            });
            delete supplier.supplierId;
            return <Supplier { ...supplier } />
          })}
        </tbody>
      </table>
    );
  }
}