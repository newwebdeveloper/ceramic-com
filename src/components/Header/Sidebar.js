import React from 'react';
import { Link } from 'react-router';

export default class Sidebar extends React.Component {
  componentDidMount() {
    $('#side-menu').metisMenu();
  }

  render() {
    return (
      <div className="navbar-default sidebar" role="navigation">
        <div className="sidebar-nav navbar-collapse collapse">
          <ul className="nav" id="side-menu">
            <li className="sidebar-search">
              <div className="input-group custom-search-form">
                <input type="text" className="form-control" placeholder="Search..." />
                <span className="input-group-btn">
                  <button className="btn btn-default" type="button">
                    <i className="fa fa-search" />
                  </button>
                </span>
              </div>
            </li>
            <li><Link to="/"><i className="fa fa-dashboard fa-fw" /> Dashboard</Link></li>
            <li>
              <a href="#"><i className="fa fa-users fa-fw" /> User<span className="fa arrow"></span></a>
              <ul className="nav nav-second-level collapse">
                <li><Link to="/customer">Customer</Link></li>
                <li><Link to="/staff">Staff</Link></li>
                <li><Link to="/supplier">Supplier</Link></li>
              </ul>
            </li>
            <li>
              <a href="#"><i className="fa fa-suitcase fa-fw" /> Product<span className="fa arrow"></span></a>
              <ul className="nav nav-second-level collapse">
                <li><Link to="/product/category">Product Category</Link></li>
                <li><Link to="/product">Product</Link></li>
                <li><Link to="/product/adjustment">Product Adjustment</Link></li>
              </ul>
            </li>
            <li>
              <a href="#"><i className="fa fa-money fa-fw" /> Sales<span className="fa arrow"></span></a>
              <ul className="nav nav-second-level collapse">
                <li><Link to="/sales/order">Sales Order</Link></li>
                <li><Link to="/sales/invoice">Sales Invoice</Link></li>
                <li><Link to="/sales/return">Sales Return</Link></li>
                <li><Link to="/sales/pricing">Sales Pricing</Link></li>
              </ul>
            </li>
            <li><Link to="/shipping-order"><i className="fa fa-truck fa-fw" /> Shipping Order</Link></li>
            <li>
              <a href="#"><i className="fa fa-credit-card fa-fw" /> Payment<span className="fa arrow"></span></a>
              <ul className="nav nav-second-level collapse">
                <li><Link to="/payment">Payment</Link></li>
                <li><Link to="/cash/in">Cash In</Link></li>
                <li><Link to="/cash/out">Cash Out</Link></li>
                <li><Link to="/cost-category">Cost Category</Link></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}