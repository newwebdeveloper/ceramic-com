import React from 'react';
import Navbar from './Navbar';
import Sidebar from './Sidebar';

export default class Header extends React.Component {
  render() {
    return (
      <header>
        <nav className="navbar navbar-default navbar-static-top" role="navigation" style={{marginBottom: 0}}>
          <Navbar />
          <Sidebar />
        </nav>
      </header>
    );
  }
}