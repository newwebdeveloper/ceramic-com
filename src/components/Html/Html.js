import React from 'react';
import SBAdmin2 from '../../externals/sb-admin-2';

export default class Html extends React.Component {
  static propTypes = {
    title: React.PropTypes.string,
    description: React.PropTypes.string,
    css: React.PropTypes.string,
  };

  render() {
    return (
      <html>
        <head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>{this.props.title}</title>
          <link rel="stylesheet" href="externals/css/bootstrap.min.css" />
          <link rel="stylesheet" href="externals/css/font-awesome.min.css" />
          <style type="text/css" dangerouslySetInnerHTML={{__html: SBAdmin2}} />
        </head>
        <body>
          <div id="root" dangerouslySetInnerHTML={{__html: this.props.body}} />
          <script type="text/javascript" src="externals/js/jquery.min.js"></script>
          <script type="text/javascript" src="externals/js/bootstrap.min.js"></script>
          <script type="text/javascript" src="externals/js/metisMenu.min.js"></script>
          <script type="text/javascript" src="public/app.js"></script>
        </body>
      </html>
    );
  }
}
