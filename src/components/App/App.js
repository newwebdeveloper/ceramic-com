import React from 'react';
import emptyFunction from 'fbjs/lib/emptyFunction';
import connectToStores from 'alt/utils/connectToStores';
import { canUseDOM } from 'fbjs/lib/ExecutionEnvironment';

import AuthenticationAction from '../../actions/AuthenticationAction';
import AuthenticationStore from '../../stores/AuthenticationStore';

@connectToStores
export default class App extends React.Component {
  static propTypes = {
    context: React.PropTypes.shape({
      setTitle: React.PropTypes.func,
      setStyle: React.PropTypes.func,
    }),
  };

  static childContextTypes = {
    onSetTitle: React.PropTypes.func.isRequired,
    onSetStyle: React.PropTypes.func,
  };

  static getStores() {
    return [AuthenticationStore];
  }

  static getPropsFromStores() {
    return {
      authenticationStore: AuthenticationStore.getState(),
    };
  }

  getChildContext() {
    const context = this.props.context || {};
    return {
      onSetTitle: context.setTitle || ((title) => {
        if (canUseDOM) {
          document.title = title;
        }
      }),
      onSetStyle: context.addStyle || emptyFunction,
    };
  }

  render() {
    const { authenticationStore, children } = this.props;

    if (!authenticationStore.accessToken && authenticationStore.refreshToken) {
      if (authenticationStore.loading) {
        return <div>Loading...</div>
      } else if (authenticationStore.errorMessage) {
        return <div>{authenticationStore.errorMessage}</div>
      }
    }

    return (
      <div id="app">
        {React.cloneElement(children, { authenticationStore })}
      </div>
    );
  }
}

App.auth = (nextState, replaceState) => {
  const { accessToken, refreshToken } = AuthenticationStore.getState();
  if (!accessToken) {
    if (refreshToken) {
      AuthenticationAction.refresh(refreshToken);
    } else {
      replaceState({ nextPathname: nextState.location.pathname }, '/login');
    }
  }
}

App.guest = (nextState, replaceState) => {
  const { accessToken, refreshToken } = AuthenticationStore.getState();
  if (accessToken || refreshToken) {
    replaceState({ nextPathname: nextState.location.pathname }, '/');
  }
}