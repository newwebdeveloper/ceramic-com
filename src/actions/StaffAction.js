import alt from '../alt';
import { createActions } from 'alt/utils/decorators';
import StaffService from '../services/StaffService';

@createActions(alt)
export default class StaffAction {
  constructor() {
    this.generateActions(
      'beforeSend',
      'clearMessage',
      'error',
      'find',
    );
  }

  fetch() {
    this.actions.beforeSend();

    StaffService
      .fetch()
      .then((res) => {
        this.dispatch(res.data);
      })
      .catch(this.actions.error);
  }

  create(payload) {
    this.actions.beforeSend();

    StaffService
      .create(payload)
      .then((res) => {
        this.dispatch(res.data);
      })
      .catch(this.actions.error);
  }

  update(id, payload) {
    this.actions.beforeSend();

    StaffService
      .update(id, payload)
      .then((res) => {
        this.dispatch(res.data);
      })
      .catch(this.actions.error);
  }

  delete(id) {
    this.actions.beforeSend();

    StaffService
      .delete(id)
      .then((res) => {
        this.dispatch(id);
      })
      .catch(this.actions.error);
  }
}