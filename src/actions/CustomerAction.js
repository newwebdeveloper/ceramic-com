import alt from '../alt';
import { createActions } from 'alt/utils/decorators';
import CustomerService from '../services/CustomerService';

@createActions(alt)
export default class CustomerAction {
  constructor() {
    this.generateActions(
      'beforeSend',
      'clearMessage',
      'error',
      'find',
    );
  }

  fetch() {
    this.actions.beforeSend();

    CustomerService
      .fetch()
      .then((res) => {
        this.dispatch(res.data);
      })
      .catch(this.actions.error);
  }

  onFind(id) {
    this.actions.beforeSend();

    CustomerService
      .find(id)
      .then((res) => {
        this.dispatch(res.data);
      })
      .catch(this.actions.error);
  }

  create(payload) {
    this.actions.beforeSend();

    CustomerService
      .create(payload)
      .then((res) => {
        this.dispatch(res.data);
      })
      .catch(this.actions.error);
  }

  update(id, payload) {
    this.actions.beforeSend();

    CustomerService
      .update(id, payload)
      .then((res) => {
        this.dispatch(res.data);
      })
      .catch(this.actions.error);
  }

  delete(id) {
    this.actions.beforeSend();

    CustomerService
      .delete(id)
      .then((res) => {
        this.dispatch(id);
      })
      .catch(this.actions.error);
  }
}