import alt from '../alt';
import { createActions } from 'alt/utils/decorators';
import AuthenticationService from '../services/AuthenticationService';

@createActions(alt)
export default class AuthenticationAction {
  constructor() {
    this.generateActions(
      'beforeSend',
      'clearMessage',
      'error',
    );
  }

  login(username, password) {
    this.actions.beforeSend();

    AuthenticationService
      .login(username, password)
      .then((res) => {
        const { access_token, expires_in, refresh_token } = res;
        this.dispatch({
          accessToken: access_token,
          refreshToken: refresh_token,
          accessTokenMaxAge: expires_in,
        });
      })
      .catch(this.actions.error);
  }

  refresh(refreshToken) {
    this.actions.beforeSend();

    AuthenticationService
      .refresh(refreshToken)
      .then((res) => {
        const { access_token, expires_in, refresh_token } = res;
        this.dispatch({
          accessToken: access_token,
          refreshToken: refresh_token,
          accessTokenMaxAge: expires_in,
        });
      })
      .catch(this.actions.error);
  }

  logout(token) {
    this.actions.beforeSend();

    AuthenticationService
      .logout(token)
      .then((res) => {
        this.dispatch();
      })
      .catch(this.actions.error);
  }
}