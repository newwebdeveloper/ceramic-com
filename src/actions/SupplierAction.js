import alt from '../alt';
import { createActions } from 'alt/utils/decorators';
import SupplierService from '../services/SupplierService';

@createActions(alt)
export default class SupplierAction {
  constructor() {
    this.generateActions(
      'beforeSend',
      'clearMessage',
      'error',
      'find',
    );
  }

  fetch() {
    this.actions.beforeSend();

    SupplierService
      .fetch()
      .then((res) => {
        this.dispatch(res.data);
      })
      .catch(this.actions.error);
  }

  create(payload) {
    this.actions.beforeSend();

    SupplierService
      .create(payload)
      .then((res) => {
        this.dispatch(res.data);
      })
      .catch(this.actions.error);
  }

  update(id, payload) {
    this.actions.beforeSend();

    SupplierService
      .update(id, payload)
      .then((res) => {
        this.dispatch(res.data);
      })
      .catch(this.actions.error);
  }

  delete(id) {
    this.actions.beforeSend();

    SupplierService
      .delete(id)
      .then((res) => {
        this.dispatch(id);
      })
      .catch(this.actions.error);
  }
}