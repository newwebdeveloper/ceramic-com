import alt from './alt';
import connectRedis from 'connect-redis';
import cookie from 'cookie';
// import cookieParser from 'cookie-parser';
import express from 'express';
import path from 'path';
import routes from './routes';
// import session from 'express-session';
// import redisConfig from './config/redis';
import { match, RoutingContext } from 'react-router';
import AuthenticationStore from './stores/AuthenticationStore';
import Html from './components/Html';
import Iso from 'iso';
import React from 'react';
import ReactDOM from 'react-dom/server';

const server = express();
// const RedisStore = connectRedis(session);

server.set('port', process.env.PORT || 5000);
// server.use(cookieParser());
// server.use(session({
//   store: new RedisStore(redisConfig),
//   resave: false,
//   saveUninitialized: false,
//   secret: process.env.SESSION_SECRET || 'session-secret',
//   cookie: {
//     httpOnly: true,
//     secure: true,
//   },
// }));
server.use('/public', express.static(path.join(__dirname, 'public')));
server.use('/externals', express.static(path.join(__dirname, 'externals')));
// server.use((req, res, next) => {
//   if (!req.session) {
//     return next(new Error());
//   }
//   next();
// });
server.get('*', (req, res) => {
  const { accessToken, refreshToken } = cookie.parse(req.headers.cookie || '');
  alt.bootstrap(JSON.stringify({
    AuthenticationStore: {
      accessToken,
      refreshToken,
    },
  }));
  match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
    if (error) {
      res.status(500).send(error.message);
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    } else if (renderProps) {
      const styles = [];
      const data = {
        title: '',
        body: '',
        style: '',
      };

      console.log(renderProps);

      renderProps.createElement = (Component, props) => {
        props = {
          context: {
            setTitle: (title) => data.title = title,
            setStyle: (style) => styles.push(style),
          },
          ...props,
        };
        return <Component {...props} />;
      };
      data.body = Iso.render(ReactDOM.renderToString(<RoutingContext {...renderProps} />), alt.flush());
      data.style = styles.join('');
      res.status(200).send(`<!DOCTYPE html>${ReactDOM.renderToStaticMarkup(<Html {...data} />)}`);
    } else {
      res.status(404).send('Not found')
    }
  });
});

server.listen(server.get('port'), function () {
  const {address, port} = this.address();
  console.log('Server is listening to http://%s:%s', address, port);
});
