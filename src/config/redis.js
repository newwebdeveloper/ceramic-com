const env = process.env.NODE_ENV || 'development';
const config = {
  "development": {
    "host": "127.0.0.1",
    "port": 6379,
    "pass": "",
  },
  "production": {
    "host": "pub-redis-10618.us-east-1-4.6.ec2.redislabs.com",
    "port": 10618,
    "pass": "l3Lh3p13W1Bti3qg",
  }
};

export default config[env];