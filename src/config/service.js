import url from 'url';

const env = process.env.NODE_ENV || 'development';
const config = {
  "development": {
    "protocol": "http",
    "hostname": "api.ceramic.dev",
    "port": 8080,
    "pathname": "v1",
  },
  "production": {
    "protocol": "https",
    "hostname": "ceramic-api.herokuapp.com",
    "port": 80,
    "pathname": "v1",
  }
};

export default url.format(config[env]);
