import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App';
import MainLayout from './layouts/MainLayout';
import LoginPage from './pages/LoginPage';
import HomePage from './pages/HomePage';
import CustomerPage from './pages/CustomerPage';
import StaffPage from './pages/StaffPage';
import SupplierPage from './pages/SupplierPage';
import ProductPage from './pages/ProductPage';
import ErrorPage from './pages/ErrorPage';

export default (
  <Route component={App}>
    <Route path="/login" component={LoginPage} onEnter={App.guest} />
    <Route path="/" component={MainLayout} onEnter={App.auth}>
      <IndexRoute component={HomePage} />
      <Route path="customer" component={CustomerPage}>
        <Route path="add" component={CustomerPage.Add} />
        <Route path="edit/:customerId" component={CustomerPage.Edit} />
      </Route>
      <Route path="staff" component={StaffPage}>
        <Route path="add" component={StaffPage.Add} />
        <Route path="edit/:staffId" component={StaffPage.Edit} />
      </Route>
      <Route path="supplier" component={SupplierPage}>
        <Route path="add" component={SupplierPage.Add} />
        <Route path="edit/:supplierId" component={SupplierPage.Edit} />
      </Route>
      <Route path="/product" component={ProductPage} />

      <Route path="*" component={ErrorPage}/>
    </Route>
  </Route>
);