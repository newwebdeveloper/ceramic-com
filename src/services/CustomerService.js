import request from 'superagent';
import AuthenticationStore from '../stores/AuthenticationStore';
import API_HOST from '../config/service';

class CustomerService {
  fetch() {
    return new Promise((resolve, reject) => {
      // TODO: This call of access token seems hacky, find better way
      const { accessToken } = AuthenticationStore.getState();
      request
        .get(`${API_HOST}/customer`)
        .set('Authorization', accessToken)
        .type('json')
        .end((err, res) => {
          if (err || !res.ok) {
            reject((err.response && err.response.body) || err);
          } else {
            resolve(res.body);
          }
        });
    });
  }

  find(id) {
    return new Promise((resolve, reject) => {
      console.log(id);
      // TODO: This call of access token seems hacky, find better way
      // const { accessToken } = AuthenticationStore.getState();
      // request
      //   .get(`${API_HOST}/customer/`+id)
      //   .set('Authorization', accessToken)
      //   .type('json')
      //   .end((err, res) => {
      //     if (err || !res.ok) {
      //       reject((err.response && err.response.body) || err);
      //     } else {
      //       resolve(res.body);
      //     }
      //   });
    });
  }

  create(payload) {
    return new Promise((resolve, reject) => {
      request
        .post(`${API_HOST}/customer`)
        .type('json')
        .send({ ...payload })
        .end((err, res) => {
          if (err || !res.ok) {
            reject((err.response && err.response.body) || err);
          } else {
            resolve(res.body);
          }
        });
    });
  }

  update(id, payload) {
    return new Promise((resolve, reject) => {
      request
        .put(`${API_HOST}/customer/${id}`)
        .type('json')
        .send({ ...payload })
        .end((err, res) => {
          if (err || !res.ok) {
            reject((err.response && err.response.body) || err);
          } else {
            resolve(res.body);
          }
        });
    });
  }

  remove(id) {
    return new Promise((resolve, reject) => {
      request
        .delete(`${API_HOST}/customer/${id}`)
        .type('json')
        .end((err, res) => {
          if (err || !res.ok) {
            reject((err.response && err.response.body) || err);
          } else {
            resolve(res.body);
          }
        });
    });
  }
}

export default new CustomerService();