import request from 'superagent';
import API_HOST from '../config/service';

class AuthenticationService {
  login(username, password) {
    const grant_type = 'password';
    return new Promise((resolve, reject) => {
      request
        .post(`${API_HOST}/oauth/token`)
        .type('json')
        .send({ username, password, grant_type })
        .end((err, res) => {
          if (err || !res.ok) {
            reject((err.response && err.response.body) || err);
          } else {
            resolve(res.body);
          }
        });
    });
  }

  refresh(refreshToken) {
    return new Promise((resolve, reject) => {
      const refresh_token = refreshToken;
      const grant_type = 'refresh_token';
      request
        .post(`${API_HOST}/oauth/refresh`)
        .type('json')
        .send({ refresh_token, grant_type })
        .end((err, res) => {
          if (err || !res.ok) {
            reject((err.response && err.response.body) || err);
          } else {
            resolve(res.body);
          }
        });
    });
  }

  logout(token) {
    return new Promise((resolve, reject) => {
      request
        .post(`${API_HOST}/oauth/revoke`)
        .type('json')
        .send({ token })
        .end((err, res) => {
          if (err || !res.ok) {
            reject((err.response && err.response.body) || err);
          } else {
            resolve(res.body);
          }
        });
    });
  }
}

export default new AuthenticationService();