import _ from 'lodash';
import alt from '../alt';
import { createStore } from 'alt/utils/decorators';
import BaseStore from './BaseStore';
import CustomerAction from '../actions/CustomerAction';

@createStore(alt)
export default class CustomerStore extends BaseStore {
  constructor() {
    super();
    this.customers = [];

    this.bindListeners({
      onBeforeSend: CustomerAction.BEFORE_SEND,
      onError: CustomerAction.ERROR,
      onClearMessage: CustomerAction.CLEAR_MESSAGE,
      onFind: CustomerAction.FIND,
      onFetch: CustomerAction.FETCH,
      onCreate: CustomerAction.CREATE,
      onUpdate: CustomerAction.UPDATE,
      onDelete: CustomerAction.DELETE,
    });
  }

  indexOf(customerId) {
    return _.findIndex(this.customers, (customer) => customer.customerId === customerId);
  }

  onFind(customerId) {
    return _.find(this.customers, (customer) => customer.customerId === customerId);
  }

  onFetch(customers) {
    this.customers = customers;
    this.onClearMessage();
    this.complete();
  }

  onCreate(customer) {
    this.customers.push(customer);
    this.success('Success add new customer');
  }

  onUpdate(customer) {
    const index = this.indexOf(customer.customerId);
    if (index < 0) return;

    _.merge(this.customers[index], customer);
    this.success('Success update customer');
  }

  onDelete(customerId) {
    const index = this.indexOf(customerId);
    if (index < 0) return;

    this.customers.splice(index, 1);
    this.success('Success remove customer');
  }
}