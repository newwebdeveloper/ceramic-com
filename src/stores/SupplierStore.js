import _ from 'lodash';
import alt from '../alt';
import { createStore } from 'alt/utils/decorators';
import BaseStore from './BaseStore';
import SupplierAction from '../actions/SupplierAction';

@createStore(alt)
export default class SupplierStore extends BaseStore {
  constructor() {
    super();
    this.suppliers = [];

    this.bindListeners({
      onBeforeSend: SupplierAction.BEFORE_SEND,
      onError: SupplierAction.ERROR,
      onClearMessage: SupplierAction.CLEAR_MESSAGE,
      onFind: SupplierAction.FIND,
      onFetch: SupplierAction.FETCH,
      onCreate: SupplierAction.CREATE,
      onUpdate: SupplierAction.UPDATE,
      onDelete: SupplierAction.DELETE,
    });
  }

  indexOf(supplierId) {
    return _.findIndex(this.suppliers, (supplier) => supplier.supplierId === supplierId);
  }

  onFind(supplierId) {
    return _.find(this.suppliers, (supplier) => supplier.supplierId === supplierId);
  }

  onFetch(suppliers) {
    this.suppliers = suppliers;
    this.onClearMessage();
    this.complete();
  }

  onCreate(supplier) {
    this.suppliers.push(supplier);
    this.success('Success add new supplier');
  }

  onUpdate(supplier) {
    const index = this.indexOf(supplier.supplierId);
    if (index < 0) return;

    _.merge(this.suppliers[index], supplier);
    this.success('Success update supplier');
  }

  onDelete(supplierId) {
    const index = this.indexOf(supplierId);
    if (index < 0) return;

    this.suppliers.splice(index, 1);
    this.success('Success remove supplier');
  }
}