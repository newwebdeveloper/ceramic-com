import _ from 'lodash';
import alt from '../alt';
import { createStore } from 'alt/utils/decorators';
import BaseStore from './BaseStore';
import StaffAction from '../actions/StaffAction';

@createStore(alt)
export default class StaffStore extends BaseStore {
  constructor() {
    super();
    this.staffs = [];

    this.bindListeners({
      onBeforeSend: StaffAction.BEFORE_SEND,
      onError: StaffAction.ERROR,
      onClearMessage: StaffAction.CLEAR_MESSAGE,
      onFind: StaffAction.FIND,
      onFetch: StaffAction.FETCH,
      onCreate: StaffAction.CREATE,
      onUpdate: StaffAction.UPDATE,
      onDelete: StaffAction.DELETE,
    });
  }

  indexOf(staffId) {
    return _.findIndex(this.staffs, (staff) => staff.staffId === staffId);
  }

  onFind(staffId) {
    return _.find(this.staffs, (staff) => staff.staffId === staffId);
  }

  onFetch(staffs) {
    this.staffs = staffs;
    this.onClearMessage();
    this.complete();
  }

  onCreate(staff) {
    this.staffs.push(staff);
    this.success('Success add new staff');
  }

  onUpdate(staff) {
    const index = this.indexOf(staff.staffId);
    if (index < 0) return;

    _.merge(this.staffs[index], staff);
    this.success('Success update staff');
  }

  onDelete(staffId) {
    const index = this.indexOf(staffId);
    if (index < 0) return;

    this.staffs.splice(index, 1);
    this.success('Success remove staff');
  }
}