import _ from 'lodash';
import alt from '../alt';
import cookie from 'cookie';
import { createStore } from 'alt/utils/decorators';
import BaseStore from './BaseStore';
import AuthenticationAction from '../actions/AuthenticationAction';

@createStore(alt)
export default class AuthenticationStore extends BaseStore {
  constructor() {
    super();
    this.accessToken = null;
    this.refreshToken = null;

    this.bindListeners({
      onBeforeSend: AuthenticationAction.BEFORE_SEND,
      onError: AuthenticationAction.ERROR,
      onClearMessage: AuthenticationAction.CLEAR_MESSAGE,
      onAuth: [AuthenticationAction.LOGIN, AuthenticationAction.REFRESH],
      onLogout: AuthenticationAction.LOGOUT,
    });
  }

  onAuth({ accessToken, refreshToken, accessTokenMaxAge }) {
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
    this.success();
    this.complete();

    const accessTokenExpires = new Date();
    accessTokenExpires.setTime(accessTokenExpires.getTime() + accessTokenMaxAge);
    document.cookie = cookie.serialize('accessToken', accessToken, {
      maxAge: accessTokenMaxAge,
      expires: accessTokenExpires,
    });

    const refreshTokenExpires = new Date();
    const refreshTokenMaxAge = 1209600;
    refreshTokenExpires.setTime(refreshTokenExpires.getTime() + refreshTokenMaxAge);
    document.cookie = cookie.serialize('refreshToken', refreshToken, {
      maxAge: refreshTokenMaxAge,
      expires: refreshTokenExpires
    });
  }

  onLogout() {
    const maxAge = 0;
    const expires = new Date(maxAge);
    document.cookie = cookie.serialize('accessToken', '', { maxAge, expires });
    document.cookie = cookie.serialize('refreshToken', '', { maxAge, expires });
  }
}