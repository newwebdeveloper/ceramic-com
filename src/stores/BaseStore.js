export default class BaseStore {
  constructor() {
    this.loading = false;
    this.errorMessage = null;
    this.successMessage = null;
  }

  success(successMessage = null) {
    this.errorMessage = null;
    this.successMessage = successMessage;
    this.complete();
  }

  complete() {
    this.loading = false;
  }

  onBeforeSend() {
    this.loading = true;
  }

  onError(error = {}) {
    this.successMessage = null;
    this.errorMessage = error.error_description || error.message;
    this.complete();
  }

  onClearMessage() {
    this.successMessage = null;
    this.errorMessage = null;
  }
}