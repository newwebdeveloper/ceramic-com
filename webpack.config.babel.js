import webpack from 'webpack';
import path from 'path';
import _ from 'lodash';

const DEBUG = !process.argv.includes('release');
const VERBOSE = process.argv.includes('verbose');

const commonConfig = {
  output: {
    publicPath: '/'
  },
  cache: DEBUG,
  debug: DEBUG,
  stats: {
    colors: true,
    reasons: DEBUG,
    hash: VERBOSE,
    version: VERBOSE,
    timings: VERBOSE,
    chunks: VERBOSE,
    chunkModules: VERBOSE,
    cached: VERBOSE,
    cachedAssets: VERBOSE
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json']
  },
  module: {
    loaders: [
      {
        test: /\.gif$/,
        loader: 'url?limit=10000&mimetype=image/gif'
      }, {
        test: /\.jpg$/,
        loader: 'url?limit=10000&mimetype=image/jpg'
      }, {
        test: /\.png$/,
        loader: 'url?limit=10000&mimetype=image/png'
      }, {
        test: /\.ico$/,
        loader: 'url?limit=10000&mimetype=image/x-icon'
      }, {
        test: /\.woff/,
        loader: 'url?mimetype=application/font-woff'
      }, {
        test: /\.ttf/,
        loader: 'url?mimetype=application/x-font-ttf'
      }, {
        test: /\.eot/,
        loader: 'url?mimetype=application/octet-stream'
      }, {
        test: /\.svg/,
        loader: 'url?mimetype=image/svg+xml'
      }, {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel?optional=runtime&stage=0'
      }
    ]
  },
  plugins: {

  }
};

const appConfig = _.merge({}, commonConfig, {
  name: 'client',
  target: 'web',
  entry: {
    app: ['./src/app']
  },
  resolve: {
    moduleDirectories: ['src', 'node_modules']
  },
  output: {
    path: path.resolve(__dirname, 'build', 'public'),
    filename: '[name].js'
  },
  devtool: DEBUG ? 'cheap-module-eval-source-map' : false,
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: 'style/useable!css'
      }, {
        test: /\.less$/,
        loader: 'style/useable!css!less'
      }
    ]
  },
  plugins: [
    new webpack.NoErrorsPlugin()
  ]
});

const serverConfig = _.merge({}, commonConfig, {
  name: 'server',
  target: 'node',
  entry: './src/server.js',
  externals: [
    (context, request, cb) => {
      cb(null, /^[a-z][a-z\/\.\-0-9]*$/i.test(request));
    }
  ],
  output: {
    path: './build',
    filename: 'server.js',
    libraryTarget: 'commonjs2'
  },
  node: {
    console: false,
    global: false,
    process: false,
    Buffer: false,
    __filename: false,
    __dirname: false
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: 'css'
      }, {
        test: /\.less$/,
        loader: 'css!less'
      }
    ]
  }
});

export default [appConfig, serverConfig];
